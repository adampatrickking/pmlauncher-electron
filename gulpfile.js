// include gulp
var gulp = require('gulp');

// include plug-ins
var jshint = require('gulp-jshint');
var minifyHTML = require('gulp-minify-html');
var concat = require('gulp-recursive-concat');
var stripDebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-clean-css');

// JS hint task
gulp.task('jshint', function() {
  gulp.src('./src/scripts/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// minify new or changed HTML pages
gulp.task('htmlpage', function() {
  var htmlSrc = './src/*.html',
      htmlDst = './build';

  gulp.src(htmlSrc)
    .pipe(minifyHTML())
    .pipe(gulp.dest(htmlDst));
});

// JS concat, strip debugging and minify
// gulp.task('scripts', function() {
//  gulp.src(['./src/scripts/lib.js','./src/scripts/*.js'])
//    .pipe(concat('script.js'))
//    .pipe(stripDebug())
//    .pipe(uglify())
//    .pipe(gulp.dest('./build/scripts/'));
// });

gulp.task('concatenationPass1', function(){
	return gulp.src('src/**/*.js')
		.pipe(recursiveConcat({extname: ".js"}))
		.pipe(gulp.dest('./build/scripts/'));

gulp.task('concatenationPass2', function(){
	return gulp.src('src/**/**/**/*.js')
		.pipe(recursiveConcat({extname: ".js"}))
		.pipe(gulp.dest('./build/scripts/'));

// CSS concat, auto-prefix and minify
gulp.task('styles', function() {
  gulp.src(['./src/stylesheets/*.css'])
    .pipe(concat('styles.css'))
    .pipe(autoprefix('last 2 versions'))
    .pipe(minifyCSS())
    .pipe(gulp.dest('./build/styles/'));
});

// default gulp task
gulp.task('default', ['htmlpage', 'concatenationPass1', 'concatenationPass2', 'styles'], function() {
  // watch for HTML changes
  gulp.watch('./src/*.html', function() {
    gulp.run('htmlpage');
  });

  // watch for JS changes
  gulp.watch('./src/scripts/*.js', function() {
    gulp.run('jshint', 'concatenationPass1');
  });

  gulp.watch('./src/**/**/scripts/*.js', function() {
    gulp.run('jshint', 'concatenationPass2');
  });

  // watch for CSS changes
  gulp.watch('./src/stylesheets/*.css', function() {
    gulp.run('styles');
  });

//  gulp.watch('./src/**/**/styles/*.css', function() {
//    gulp.run('styles-recursive');
//  });
});
