'use strict';

var ipc = require('electron').ipcRenderer;
var fs = require('fs');

var me = document.currentScript;

class MainWindowView extends View
{
    constructor()
    {
        super();

        //class fields
        var closeButton;
        var minimizeButton;

        // Add components
        this.appendComponent(this.directory+'header', 'body');
        this.appendComponent(this.directory+'navigation', 'body');
        this.appendComponent(this.directory+'content', 'body');

        closeButton = $('#windowClose');
        minimizeButton = $('#windowMinimize');

        // Add event listeners
        closeButton.addEventListener('click', onWindowClose);
        minimizeButton.addEventListener('click', onWindowMinimize);
    }
}
