'use strict';

class Utils
{
    constructor()
    {

    }

    static parseElements(elementStr)
    {
        var div, elements;

        div = document.createElement('div');
        div.innerHTML = elementStr;
        elements = div.childNodes;

        return elements;
    }

    
}
