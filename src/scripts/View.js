'use strict';

var me = document.currentScript;

// Load the utils class from the same directory as this class and place it
// before this class in the head
include("Utils", "global", me);

class View
{
    constructor()
    {

    }

    appendComponent(file, destination)
    {
        var contents, target, elements;

        if (destination === "body") {
            target = document.body;
        } else {
            target = $('#'+destination);
        }

        contents = fs.readFileSync(file+'.html', 'utf8');
        elements = Utils.parseElements(contents);

        for (var i=0;i<elements.length;i++) {
            target.appendChild(elements[i]);
        }
    }

    get directory()
    {
        return __dirname + '/';
    }

    get filepath()
    {
        return __filename;
    }
}
