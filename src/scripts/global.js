var ipc = require('electron').ipcRenderer;
var fs = require('fs');

function $(indentifier)
{
    var type, name, element;

    if (indentifier < 1)
        return;

    type = indentifier[0];
    name = indentifier.substring(1);

    if (type === '#')
        element = document.getElementById(name);
    else if (type === '.')
        element = document.getElementsByClassName(name)[0];
    else if (name === 'body')
        element = document.body;

    return element;
}

function include(name, scope, loadBefore)
{
    var scripts, element, aboveOther;

    // By default, add the script to the bottom of the head
    aboveOther = false;

    // Determine the scope, local by default
    if (scope == 'local')
        name = "./scripts/" + name + ".js";
    else if (scope == 'global')
        name = "../../scripts/" + name + ".js";
    else if (scope == 'direct')
        name = "./" + name + ".js";
    else
        name = "./scripts/" + name + ".js";

    // Get all of the nodes with the tag name 'script'
    scripts = document.getElementsByTagName('script');

    // Ensure the name variable has a value
    if (name.length < 0)
        return;

    // Check whether the script has already been imported
    for (var i = 0; i < scripts.length; i++) {
        var src = scripts[i].getAttribute('src');

        if (src === name)
            return;
    }

    // If there is a specified element to load before,
    // make sure to insert it before that imported
    if (loadBefore != null && loadBefore != undefined) {
        aboveOther = true;
    }

    // If there are no problems, append to the head
    element = document.createElement('script');
    element.setAttribute("src", name);
    element.setAttribute("type","text/javascript");

    if (aboveOther)
        document.getElementsByTagName('head')[0].insertBefore(element, loadBefore);
    else
        document.getElementsByTagName('head')[0].appendChild(element);
}

function onWindowClose()
{
    ipc.send('asynchronous-close');
}

function onWindowMinimize()
{
    ipc.send('asynchronous-minimize');
}
